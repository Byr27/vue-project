import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import VeeValidate from 'vee-validate';
import VueMoment from 'vue-moment';

import App from './App';
import router from './router';
import store from './store';

import VeeValidateMessages from './global/js/veeValidateMessages';

Vue.use(VueMoment);
Vue.use(Vuex);
Vue.use(VueResource);
Vue.use(VeeValidate, {
  events: 'change',
  locale: 'ru',
  dictionary: {
    ru: {
      messages: VeeValidateMessages,
    },
  },
});

Vue.config.devtools = true;

const app = new Vue({
  store,
  router,
  render: h => h(App),
});

app.$mount('#app');
