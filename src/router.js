import Vue from 'vue';
import VueRouter from 'vue-router';

import Sudoku from '@pages/Sudoku';
import Tests from '@pages/Tests';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      path: '/',
      component: Sudoku,
    },
    {
      path: '/tests',
      component: Tests,
    },
  ],
});
