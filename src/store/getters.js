export default {
  wallets: state => state.dictionary.wallets,
  users: state => state.dictionary.users,
  testPassword: state => state.test.settings.password,
  mixedTests: state => state.test.questions.sort(() => Math.random() - 0.5),
};
