export default {
  getDictionaries({ commit }) {
    const list = localStorage.getItem('list');

    commit('updateState', {
      list: list
        ? JSON.parse(list)
        : [],
      dictionary: {
        wallets: [
          {
            id: 1,
            name: 'USD',
          },
          {
            id: 2,
            name: 'UAH',
          },
          {
            id: 3,
            name: 'Payoneer (USD)',
          },
          {
            id: 4,
            name: 'Mediator (USD)',
          },
        ],
        users: [
          {
            id: 1,
            name: 'Pavel',
          },
          {
            id: 2,
            name: 'Misha',
          },
        ],
      },
    });
  },

  saveData({ commit, state }, item) {
    const list = [
      item,
      ...state.list,
    ];

    localStorage.setItem('list', JSON.stringify(list));
    commit('updateState', { list });
  },
};
