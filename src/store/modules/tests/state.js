export default {
  test: {
    settings: {
      password: '1234',
    },
    questions: [
      {
        id: 1,
        question: '2+2',
        answers: [
          {
            name: 4,
            is: 1,
          },
          {
            name: 5,
            is: 'q',
          },
          {
            name: 1,
            is: 'w',
          },
          {
            name: 7,
            is: 'e',
          },
        ],
      },
      {
        id: 2,
        question: '8 + 1',
        answers: [
          {
            name: 5,
            is: 'q',
          },
          {
            name: 1,
            is: 'w',
          },
          {
            name: 9,
            is: 1,
          },
          {
            name: 7,
            is: 'e',
          },
        ],
      },
      {
        id: 3,
        question: '4 + 1 + 3',
        answers: [
          {
            name: 5,
            is: 'q',
          },
          {
            name: 8,
            is: 1,
          },
          {
            name: 1,
            is: 'w',
          },
          {
            name: 7,
            is: 'e',
          },
        ],
      },
    ],
  },
};
