export default {
  testPassword: state => state.test.settings.password,
  mixedTests: state => state.test.questions.sort(() => Math.random() - 0.5),
};
