import Vue from 'vue';
import Vuex, { Store } from 'vuex';

import sudoku from './modules/sudoku';
import tests from './modules/tests';
import cashbalance from './modules/cashbalance';

Vue.use(Vuex);

export default new Store({
  modules: {
    sudoku,
    tests,
    cashbalance,
  },
});
